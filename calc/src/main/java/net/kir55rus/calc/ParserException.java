package net.kir55rus.calc;

/**
 * Created by kir55rus on 12.02.17.
 */
public class ParserException extends IllegalArgumentException {
    public ParserException() {
        super();
    }

    public ParserException(String msg) {
        super(msg);
    }
}

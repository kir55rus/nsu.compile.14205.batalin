package net.kir55rus.calc;

/**
 * Created by kir55rus on 12.02.17.
 */
public class Parser {
    private Lexer lexer;
    private Lexeme lexeme;

    public Parser(Lexer lexer) {
        this.lexer = lexer;
    }

    public int calculate() {
        int result;

        try {
            lexeme = lexer.getLexeme();
            if (lexeme == null) {
                return 0;
            }

            result = parseExpression();
        } catch (Exception e) {
            throw new ParserException(e.getMessage());
        }

        if (lexeme != null) {
            throw new ParserException("Bad format");
        }

        return result;
    }

    private int parseExpression() {
        int total = parseTerm();

        while (lexeme != null && (lexeme.getLexemeType() == LexemeType.PLUS || lexeme.getLexemeType() == LexemeType.MINUS)) {
            int sign = lexeme.getLexemeType() == LexemeType.PLUS ? 1 : -1;

            lexeme = lexer.getLexeme();
            if (lexeme == null) {
                throw new ParserException("Bad format");
            }

            total += sign * parseTerm();
        }

        return total;
    }

    private int parseTerm() {
        int total = parseFactor();

        while (lexeme != null && (lexeme.getLexemeType() == LexemeType.MULTIPLICATION || lexeme.getLexemeType() == LexemeType.DIVISION)) {
            LexemeType type = lexeme.getLexemeType();

            lexeme = lexer.getLexeme();
            if (lexeme == null) {
                throw new ParserException("Bad format");
            }

            int value = parseTerm();
            total = type == LexemeType.DIVISION ? total / value : total * value;
        }

        return total;
    }

    private int parseFactor() {
        int total = parsePower();

        if (lexeme == null || lexeme.getLexemeType() != LexemeType.POWER) {
            return total;
        }

        lexeme = lexer.getLexeme();
        if (lexeme == null) {
            throw new ParserException("Bad format");
        }

        int factor = parseFactor();
        return (int)Math.pow(total, factor);
    }

    private int parsePower() {
        if (lexeme.getLexemeType() == LexemeType.MINUS) {
            lexeme = lexer.getLexeme();
            if (lexeme == null) {
                throw new ParserException("Bad format");
            }

            return -1 * parseAtom();
        }

        return parseAtom();
    }

    private int parseAtom() {
        if (lexeme.getLexemeType() == LexemeType.LEFT_BRACKET) {
            lexeme = lexer.getLexeme();
            if (lexeme == null) {
                throw new ParserException("Bad format");
            }

            int result = parseExpression();

            if (lexeme == null || lexeme.getLexemeType() != LexemeType.RIGHT_BRACKET) {
                throw new ParserException("Bad format");
            }

            lexeme = lexer.getLexeme();

            return result;
        }

        int result = Integer.valueOf(lexeme.getSymbols());
        lexeme = lexer.getLexeme();
        return result;
    }
}

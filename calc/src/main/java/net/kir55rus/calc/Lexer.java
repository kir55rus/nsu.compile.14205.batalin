package net.kir55rus.calc;

import java.io.IOException;
import java.io.Reader;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by kir55rus on 12.02.17.
 */
public class Lexer {
    private Reader reader;
    private char[] buffer;
    private Map<Character, LexemeType> lexemeTypeMap = new TreeMap<Character, LexemeType>();

    public Lexer(Reader reader) {
        this.reader = reader;

        lexemeTypeMap.put('+', LexemeType.PLUS);
        lexemeTypeMap.put('-', LexemeType.MINUS);
        lexemeTypeMap.put('*', LexemeType.MULTIPLICATION);
        lexemeTypeMap.put('/', LexemeType.DIVISION);
        lexemeTypeMap.put('(', LexemeType.LEFT_BRACKET);
        lexemeTypeMap.put(')', LexemeType.RIGHT_BRACKET);
        lexemeTypeMap.put('^', LexemeType.POWER);
    }

    public Lexeme getLexeme() {
        Lexeme lexeme = null;

        try {
            StringBuilder builder = new StringBuilder();
            boolean hasData = true;
            if (buffer == null) {
                buffer = new char[1];
                hasData = reader.read(buffer) != -1;
            }

            parseCycle:
            while (hasData) {
                char symb = buffer[0];

                switch (symb) {
                    case ' ':
                        if (builder.length() > 0) {
                            break parseCycle;
                        } else {
                            break;
                        }

                    case '+':
                    case '-':
                    case '*':
                    case '/':
                    case '(':
                    case ')':
                    case '^':
                        if (builder.length() == 0) {
                            lexeme = new Lexeme(lexemeTypeMap.get(symb), String.valueOf(symb));
                            hasData = reader.read(buffer) != -1;
                        }
                        break parseCycle;

                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        builder.append(symb);
                        break;

                    default:
                        throw new IllegalArgumentException("Bad symbol: " + symb);
                }

                hasData = reader.read(buffer) != -1;
            }

            if (builder.length() > 0) {
                lexeme = new Lexeme(LexemeType.NUMERIC, builder.toString());
            }

            if (!hasData) {
                buffer = null;
            }

        } catch (IOException e) {
            System.err.println("Can't read: " + e.getMessage());
            return null;
        }

        return lexeme;
    }

    private boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
        } catch(Exception e) {
            return false;
        }
        return true;
    }
}

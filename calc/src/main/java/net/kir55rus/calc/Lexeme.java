package net.kir55rus.calc;

/**
 * Created by kir55rus on 12.02.17.
 */
public class Lexeme {
    private LexemeType lexemeType;
    private String symbols;

    public Lexeme(LexemeType type, String str) {
        lexemeType = type;
        symbols = str;
    }

    public LexemeType getLexemeType() {
        return lexemeType;
    }

    public String getSymbols() {
        return symbols;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Lexeme) {
            Lexeme otherLexeme = (Lexeme) other;
            return lexemeType.equals(otherLexeme.getLexemeType()) && symbols.equals(otherLexeme.getSymbols());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return symbols.hashCode();
    }
}

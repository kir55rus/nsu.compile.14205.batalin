package net.kir55rus.calc;

/**
 * Created by kir55rus on 12.02.17.
 */
public enum LexemeType {
    PLUS,
    MINUS,
    MULTIPLICATION,
    DIVISION,
    LEFT_BRACKET,
    RIGHT_BRACKET,
    POWER,
    NUMERIC;

}

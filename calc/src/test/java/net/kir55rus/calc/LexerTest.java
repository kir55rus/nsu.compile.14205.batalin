package net.kir55rus.calc;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by kir55rus on 12.02.17.
 */
public class LexerTest extends Assert {

    private void testData(String input, Lexeme[] answer, String msg) {
        Reader reader = new StringReader(input);
        ArrayList<Lexeme> expected = new ArrayList<Lexeme>(Arrays.asList(answer));
        ArrayList<Lexeme> actual = new ArrayList<Lexeme>();

        Lexer lexer = new Lexer(reader);
        Lexeme lexeme;
        while ((lexeme = lexer.getLexeme()) != null) {
            actual.add(lexeme);
        }

        assertEquals(actual, expected, msg);
    }

    @Test
    public void testNumbers() {
        Lexeme[] expected = {
                new Lexeme(LexemeType.NUMERIC, "12"),
                new Lexeme(LexemeType.NUMERIC, "3"),
                new Lexeme(LexemeType.NUMERIC, "4"),
        };

        testData("12 3 4", expected, "Just numbers");
    }

    @Test
    public void testNumberWithSpaces() {
        Lexeme[] expected = {
                new Lexeme(LexemeType.NUMERIC, "12"),
                new Lexeme(LexemeType.NUMERIC, "3"),
                new Lexeme(LexemeType.NUMERIC, "4"),
                new Lexeme(LexemeType.NUMERIC, "67"),
        };

        testData("     12   3  4  67 ", expected, "Numbers with spaces");
    }

    @Test
    public void testNotNumbers() {
        Lexeme[] expected = {
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.PLUS, "+"),
                new Lexeme(LexemeType.RIGHT_BRACKET, ")"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.MULTIPLICATION, "*"),
                new Lexeme(LexemeType.MINUS, "-"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.POWER, "^"),
                new Lexeme(LexemeType.PLUS, "+"),
        };

        testData("(+)(//*-(/^+", expected, "Not numbers");
    }

    @Test
    public void testNotNumbersWithSpaces() {
        Lexeme[] expected = {
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.PLUS, "+"),
                new Lexeme(LexemeType.RIGHT_BRACKET, ")"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.MULTIPLICATION, "*"),
                new Lexeme(LexemeType.MINUS, "-"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.POWER, "^"),
                new Lexeme(LexemeType.PLUS, "+"),
        };

        testData("   ( +) ( / /*  -(/     ^    +   ", expected, "Not numbers with spaces");
    }

    @Test
    public void testNumbersWithNotNumbers1() {
        //Start with number, end with not number
        Lexeme[] expected = {
                new Lexeme(LexemeType.NUMERIC, "45"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "4"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "64"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "7"),
                new Lexeme(LexemeType.RIGHT_BRACKET, ")"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "0"),
                new Lexeme(LexemeType.MINUS, "-"),
                new Lexeme(LexemeType.NUMERIC, "99"),
                new Lexeme(LexemeType.PLUS, "+"),
                new Lexeme(LexemeType.NUMERIC, "123"),
                new Lexeme(LexemeType.POWER, "^"),
                new Lexeme(LexemeType.MULTIPLICATION, "*"),
        };

        testData("45(4/64/(7)/0-99+123^*", expected, "Numbers with not numbers #1");
    }

    @Test
    public void testNumbersWithNotNumbers2() {
        //Start with number, end with number
        Lexeme[] expected = {
                new Lexeme(LexemeType.NUMERIC, "45"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "4"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "64"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "7"),
                new Lexeme(LexemeType.RIGHT_BRACKET, ")"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "0"),
                new Lexeme(LexemeType.MINUS, "-"),
                new Lexeme(LexemeType.NUMERIC, "99"),
                new Lexeme(LexemeType.PLUS, "+"),
                new Lexeme(LexemeType.NUMERIC, "123"),
                new Lexeme(LexemeType.POWER, "^"),
                new Lexeme(LexemeType.MULTIPLICATION, "*"),
                new Lexeme(LexemeType.NUMERIC, "12"),
        };

        testData("45(4/64/(7)/0-99+123^*12", expected, "Numbers with not numbers #2");
    }

    @Test
    public void testNumbersWithNotNumbers3() {
        //Start with not number, end with number
        Lexeme[] expected = {
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "4"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "64"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "7"),
                new Lexeme(LexemeType.RIGHT_BRACKET, ")"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "0"),
                new Lexeme(LexemeType.MINUS, "-"),
                new Lexeme(LexemeType.NUMERIC, "99"),
                new Lexeme(LexemeType.PLUS, "+"),
                new Lexeme(LexemeType.NUMERIC, "123"),
                new Lexeme(LexemeType.POWER, "^"),
                new Lexeme(LexemeType.MULTIPLICATION, "*"),
                new Lexeme(LexemeType.NUMERIC, "12"),
        };

        testData("(4/64/(7)/0-99+123^*12", expected, "Numbers with not numbers #3");
    }

    @Test
    public void testNumbersWithNotNumbers4() {
        //Start with not number, end with not number
        Lexeme[] expected = {
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "4"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "64"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "7"),
                new Lexeme(LexemeType.RIGHT_BRACKET, ")"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "0"),
                new Lexeme(LexemeType.MINUS, "-"),
                new Lexeme(LexemeType.NUMERIC, "99"),
                new Lexeme(LexemeType.PLUS, "+"),
                new Lexeme(LexemeType.NUMERIC, "123"),
                new Lexeme(LexemeType.POWER, "^"),
                new Lexeme(LexemeType.MULTIPLICATION, "*"),
        };

        testData("(4/64/(7)/0-99+123^*", expected, "Numbers with not numbers #4");
    }

    @Test
    public void testNumbersWithNotNumbersSpace1() {
        //Start with number, end with not number
        Lexeme[] expected = {
                new Lexeme(LexemeType.NUMERIC, "45"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "4"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "64"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "7"),
                new Lexeme(LexemeType.RIGHT_BRACKET, ")"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "0"),
                new Lexeme(LexemeType.MINUS, "-"),
                new Lexeme(LexemeType.NUMERIC, "99"),
                new Lexeme(LexemeType.PLUS, "+"),
                new Lexeme(LexemeType.NUMERIC, "1"),
                new Lexeme(LexemeType.NUMERIC, "23"),
                new Lexeme(LexemeType.POWER, "^"),
                new Lexeme(LexemeType.MULTIPLICATION, "*"),
        };

        testData("  45  (4/64 / ( 7 )/0-99 +  1   23^*   ", expected, "Numbers with not numbers space #1");
    }

    @Test
    public void testNumbersWithNotNumbersSpace2() {
        //Start with number, end with number
        Lexeme[] expected = {
                new Lexeme(LexemeType.NUMERIC, "45"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "4"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "64"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "7"),
                new Lexeme(LexemeType.RIGHT_BRACKET, ")"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "0"),
                new Lexeme(LexemeType.MINUS, "-"),
                new Lexeme(LexemeType.NUMERIC, "99"),
                new Lexeme(LexemeType.PLUS, "+"),
                new Lexeme(LexemeType.NUMERIC, "123"),
                new Lexeme(LexemeType.POWER, "^"),
                new Lexeme(LexemeType.MULTIPLICATION, "*"),
                new Lexeme(LexemeType.NUMERIC, "12"),
        };

        testData("  45   (4/  64/   ( 7 )/  0- 99  +  123   ^* 12   ", expected, "Numbers with not numbers space #2");
    }

    @Test
    public void testNumbersWithNotNumbersSpace3() {
        //Start with not number, end with number
        Lexeme[] expected = {
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "4"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "64"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "7"),
                new Lexeme(LexemeType.RIGHT_BRACKET, ")"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "0"),
                new Lexeme(LexemeType.MINUS, "-"),
                new Lexeme(LexemeType.NUMERIC, "99"),
                new Lexeme(LexemeType.PLUS, "+"),
                new Lexeme(LexemeType.NUMERIC, "123"),
                new Lexeme(LexemeType.POWER, "^"),
                new Lexeme(LexemeType.MULTIPLICATION, "*"),
                new Lexeme(LexemeType.NUMERIC, "12"),
        };

        testData("(   4/   64/(7   )/0-   99 +123 ^ * 12    ", expected, "Numbers with not numbers space #3");
    }

    @Test
    public void testNumbersWithNotNumbersSpace4() {
        //Start with not number, end with not number
        Lexeme[] expected = {
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "4"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "64"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.LEFT_BRACKET, "("),
                new Lexeme(LexemeType.NUMERIC, "7"),
                new Lexeme(LexemeType.RIGHT_BRACKET, ")"),
                new Lexeme(LexemeType.DIVISION, "/"),
                new Lexeme(LexemeType.NUMERIC, "0"),
                new Lexeme(LexemeType.MINUS, "-"),
                new Lexeme(LexemeType.NUMERIC, "99"),
                new Lexeme(LexemeType.PLUS, "+"),
                new Lexeme(LexemeType.NUMERIC, "123"),
                new Lexeme(LexemeType.POWER, "^"),
                new Lexeme(LexemeType.MULTIPLICATION, "*"),
        };

        testData("  ( 4/64/(    7   )/    0-99+  123^*   ", expected, "Numbers with not numbers space #4");
    }

    @Test
    public void testBadFormat1() {
        try {
            testData("32f322/3", new Lexeme[0], "Bad format #1");
            assertTrue(false, "Bad format #1");
        } catch (Exception e) {
        }
    }

    @Test
    public void testBadFormat2() {
        try {
            testData("%32322/3", new Lexeme[0], "Bad format #2");
            assertTrue(false, "Bad format #2");
        } catch (Exception e) {
        }
    }

    @Test
    public void testBadFormat3() {
        try {
            testData("32322/3_", new Lexeme[0], "Bad format #3");
            assertTrue(false, "Bad format #3");
        } catch (Exception e) {
        }
    }
}

package net.kir55rus.calc;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.StringReader;

/**
 * Created by kir55rus on 12.02.17.
 */
public class ParserTest extends Assert {

    private void testData(String input, int expected, String msg) {
        StringReader reader = new StringReader(input);
        Lexer lexer = new Lexer(reader);
        Parser parser = new Parser(lexer);

        int result = parser.calculate();
        assertEquals(result, expected, msg);
    }

    @Test
    public void testAtom() {
        testData("12", 12, "Simple atom");
        testData(" 12  ", 12, "Simple atom with spaces");
        testData("12+1", 13, "Two atoms");
        testData("12+1-5", 8, "Three atoms");
    }

    @Test
    public void testAtomBrackets() {
        testData("(12)", 12, "Atom with brackets #1");
        testData("(12)+1", 13, "Atom with brackets #2");
        testData("(12)+(1)", 13, "Atom with brackets #3");
        testData("(((12))+(1))", 13, "Atom with brackets #4");
        testData("(((12))+(1)-5)", 8, "Atom with brackets #5");
        testData("(((12))+(1)-(5))", 8, "Atom with brackets #6");
        testData("(((12))+((1)-(5)))", 8, "Atom with brackets #7");
    }

    @Test
    public void testTerm() {
        testData("1*2", 2, "Term #1");
        testData("1*2/2", 1, "Term #2");
        testData("1*(2/2)", 1, "Term #3");
        testData("1*(2/2)+5", 6, "Term #4");
        testData("1*(2/2)+5/1-8/2", 2, "Term #5");
        testData("1*(2/2)+(5/1-8/2)", 2, "Term #6");
    }

    @Test
    public void testPower() {
        testData("-5", -5, "Power #1");
        testData("-5+5", 0, "Power #2");
        testData("5-5+5", 5, "Power #3");
        testData("5+(-5+5)", 5, "Power #4");
        testData("5-(5+5)", -5, "Power #5");
        testData("-5-(5+5)", -15, "Power #6");
        testData("-5-(5+-5)", -5, "Power #7");
        testData("-5+-5", -10, "Power #8");
        testData("-5--5", 0, "Power #9");
    }

    @Test
    public void testFactor() {
        testData("4^2", 16, "Factor #1");
        testData("-4^2", 16, "Factor #2");
        testData("-2^3", -8, "Factor #3");
        testData("-(2+1)^2", 9, "Factor #4");
        testData("16^-2", 0, "Factor #5");
        testData("-1^-1", -1, "Factor #6");
    }

    @Test
    public void testFull() {
        testData("(-1*436+34)*6--6^2", -2448, "Full #1");
        testData("(-1*436+34)*6--6^3", -2196, "Full #1");
        testData("5-(-2^3)*10", 85, "Full #1");
    }

    @Test
    public void testBadFormat() {
        try {
            testData("(-1*436+34*6--6^2", 0, "Bad format #1");
            assertTrue(false, "Bad format #1");
        } catch (Exception e) {
        }

        try {
            testData("-1*436+)34*6--6^2", 0, "Bad format #2");
            assertTrue(false, "Bad format #2");
        } catch (Exception e) {
        }

        try {
            testData("-1*4(36+)*34*6--6^2", 0, "Bad format #3");
            assertTrue(false, "Bad format #3");
        } catch (Exception e) {
        }

        try {
            testData("-1*436+34*6-6^2-", 0, "Bad format #4");
            assertTrue(false, "Bad format #3");
        } catch (Exception e) {
        }

        try {
            testData("-1*436+34*6-6^", 0, "Bad format #5");
            assertTrue(false, "Bad format #3");
        } catch (Exception e) {
        }

        try {
            testData("-1*436+34*6-6^2*", -0, "Bad format #6");
            assertTrue(false, "Bad format #3");
        } catch (Exception e) {
        }
    }
}
